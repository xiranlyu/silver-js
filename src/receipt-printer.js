
export default class ReceiptPrinter {
  constructor (products) {
    this.products = [...products];
  }

  print (barcodes) {
    // TODO: Please implement the method
    // <-start-
    barcodes.sort();
    const productNames = [];
    const productQuantity = [];
    const productUnits = [];
    let totalPrice = 0;
    let productReceipt = '';
    if (barcodes.length === 0) {
      const receiptForNothing = '==================== Receipt ====================\n' +
      '\n\n' +
      '===================== Total =====================\n' +
      totalPrice.toFixed(2)
      ;
      return receiptForNothing;
    } else {
      this.products.forEach(product => {
        barcodes.forEach(barcode => {
          if (barcode === product.barcode) {
            if (!productNames.includes(product.name)) {
              productNames.push(product.name);
              productQuantity.push(1);
              productUnits.push(product.unit);
            } else {
              const index = productNames.indexOf(product.name);
              productQuantity[index] += 1;
            }
            totalPrice += product.price;
          }
        });
      });
      for (let i = 0; i < productNames.length; i++) {
        productReceipt += `${productNames[i].padEnd(30)}x${productQuantity[i]}        ${productUnits[i]}\n`;
      }
      if (totalPrice === 0) {
        throw new Error('Unknown barcode.');
      } else {
        const receipt =
        '==================== Receipt ====================\n' +
        productReceipt + '\n' +
      '===================== Total =====================\n' +
      totalPrice.toFixed(2)
      ;
        return receipt;
      }
    }
    // --end->
  }
}
